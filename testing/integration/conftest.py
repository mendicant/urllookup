import os
import pytest


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig):
    return os.path.join(
        str(pytestconfig.rootdir),
        "testing",
        "integration",
        "docker-compose-redis-cluster.yml")
