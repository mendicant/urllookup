import http
import pytest
import random
import requests
import time
from requests.exceptions import ConnectionError


BASE_PATH = 'urlinfo/v1'
DEFAULT_NETLOC = 'localhost:80'
DEFAULT_PATH = 'a/path'
DEFAULT_QUERY = 'one=1&two=2'


def is_responsive(url):
    try:
        response = requests.get(url)
        if response.status_code == http.HTTPStatus.NO_CONTENT:
            # Integration tests with docker are more stable if
            # we wait a little bit longer
            time.sleep(5)
            return True
        else:
            return False
    except ConnectionError:
        return False


@pytest.fixture(scope="session")
def app_service(docker_ip, docker_services):
    port = docker_services.port_for("url-lookup", 5555)
    base_url = "http://{}:{}".format(docker_ip, port)
    docker_services.wait_until_responsive(
        timeout=30.0, pause=1.0,
        check=lambda: is_responsive(base_url + '/healthcheck'))
    return base_url


def _make_url(app_service, netloc, original_path, query):
    return (
        '{app_service}/'
        '{base_path}/{netloc}/{original_path}?{query}'.format(
            app_service=app_service,
            base_path=BASE_PATH,
            netloc=netloc,
            original_path=original_path,
            query=query))


@pytest.mark.integration
class TestAppWithRedis():
    def _init_query_id(self):
        self._query_id = random.randint(0, 2 << 31)

    def _make_default_url(self, app_service):
        self._init_query_id()
        query = DEFAULT_QUERY + "&unique={}".format(self._query_id)
        return _make_url(app_service, DEFAULT_NETLOC, DEFAULT_PATH, query)

    def test_get_url_empty_db(self, app_service):
        response = requests.get(self._make_default_url(app_service))
        assert response.status_code == http.HTTPStatus.NOT_FOUND

    def test_get_url(self, app_service):
        """
        We can put a URL in and find it
        """
        url = self._make_default_url(app_service)
        response = requests.put(url)
        assert response.status_code == http.HTTPStatus.CREATED
        response = requests.get(url)
        assert response.status_code == http.HTTPStatus.NO_CONTENT
        response = requests.delete(url)

    def test_delete_url(self, app_service):
        """
        We can put a URL in and delete it
        """
        url = self._make_default_url(app_service)
        response = requests.put(url)
        assert response.status_code == http.HTTPStatus.CREATED
        response = requests.delete(url)
        assert response.status_code == http.HTTPStatus.NO_CONTENT
        response = requests.get(url)
        assert response.status_code == http.HTTPStatus.NOT_FOUND
