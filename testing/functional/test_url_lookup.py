import http
import pytest

from url_lookup import create_app
from url_lookup.config.config_names import (
    URL_REPOSITORY_MODULE, URL_REPOSITORY_CLASS, TESTING)


BASE_PATH = 'urlinfo/v1'
DEFAULT_NETLOC = 'localhost:80'
DEFAULT_PATH = 'a/path'
DEFAULT_QUERY = 'one=1&two=2'


def _make_url(netloc, original_path, query):
    return (
        'http://localhost/'
        '{base_path}/{netloc}/{original_path}?{query}'.format(
            base_path=BASE_PATH,
            netloc=netloc,
            original_path=original_path,
            query=query))


def _make_default_url():
    return _make_url(DEFAULT_NETLOC, DEFAULT_PATH, DEFAULT_QUERY)


@pytest.fixture
def client():
    app = create_app(
        {
            URL_REPOSITORY_MODULE: 'url_lookup.infra.inmemory_url_repository',
            URL_REPOSITORY_CLASS: 'InMemoryURLRepository',
            TESTING: True
        })
    with app.test_client() as client:
        yield client


def test_get_url_empty_db(client):
    result = client.get(_make_default_url())
    assert result.status_code == http.HTTPStatus.NOT_FOUND


def test_get_url(client):
    """
    We can put a URL in and find it
    """
    result = client.put(_make_default_url())
    assert result.status_code == http.HTTPStatus.CREATED
    result = client.get(_make_default_url())
    assert result.status_code == http.HTTPStatus.NO_CONTENT
