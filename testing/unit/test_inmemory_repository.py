import pytest
from collections import namedtuple
from url_lookup.infra.inmemory_url_repository import InMemoryURLRepository
from url_lookup.models import URL

ConfigTuple = namedtuple('ConfigTuple', ['REDIS_STARTUP_NODES'])


@pytest.fixture
def default_config():
    return ConfigTuple(REDIS_STARTUP_NODES=[])


@pytest.fixture
def default_url():
    return URL(
        netloc='localhost:80',
        path='/a/path',
        query='one=1&two=2')


@pytest.fixture
def inmemory_repo(default_config):
    return InMemoryURLRepository(default_config)


def test_add(inmemory_repo, default_url):
    result = inmemory_repo.add(default_url)
    assert result is True


def test_add_dupe(inmemory_repo, default_url):
    result = inmemory_repo.add(default_url)
    assert result is True
    result = inmemory_repo.add(default_url)
    assert result is False


def test_get_empty_repo(inmemory_repo, default_url):
    expect_false = inmemory_repo.get_url(default_url)
    assert expect_false is False


def test_get(inmemory_repo, default_url):
    added = inmemory_repo.add(default_url)
    assert added is True
    expect_true = inmemory_repo.get_url(default_url)
    assert expect_true is True


def test_delete(inmemory_repo, default_url):
    added = inmemory_repo.add(default_url)
    assert added is True
    expect_true = inmemory_repo.delete(default_url)
    assert expect_true is True
