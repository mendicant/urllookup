import pytest
from url_lookup.infra.redis_cluster_url_repository import (
    RedisClusterURLRepository)
from url_lookup.models import URL
from unittest.mock import patch
from flask import Flask


"""
Note that this doesn't actually connect to a redis cluster: it just tests
the logic around the redis calls.
"""


class MockRedis(object):
    def __init__(self):
        self.return_value = None

    def do_nothing(self):
        return self.return_value

    def set(self, key, value, nx=None):
        return self.do_nothing()

    def exists(self, key):
        return self.do_nothing()

    def delete(self, key):
        return self.do_nothing()


@pytest.fixture
def default_config():
    return dict(REDIS_STARTUP_NODES="[]")


@pytest.fixture
def default_url():
    return URL(
        netloc='localhost:80',
        path='/a/path',
        query='one=1&two=2')


@pytest.fixture
def mock_redis_client():
    with patch('url_lookup.infra.redis_cluster_url_repository.RedisCluster') as mocked_client:
        yield mocked_client


@pytest.fixture
def dummy_app():
    dummy = Flask("dummy")
    with dummy.app_context() as context:
        yield context


@pytest.fixture
def repo(default_config, mock_redis_client):
    mock_redis_client.return_value = None
    result = RedisClusterURLRepository(default_config)
    result._client = MockRedis()
    return result


def test_add(repo, default_url, dummy_app):
    repo._client.return_value = True
    result = repo.add(default_url)
    assert result is True


def test_get(repo, default_url, dummy_app):
    repo._client.return_value = True
    added = repo.add(default_url)
    assert added is True
    expect_true = repo.get_url(default_url)
    assert expect_true is True


def test_delete(repo, default_url, dummy_app):
    repo._client.return_value = True
    added = repo.add(default_url)
    assert added is True
    expect_true = repo.delete(default_url)
    assert expect_true is True
