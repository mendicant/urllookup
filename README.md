# URL Lookup service

This application stores URLs and allows lookups to determine whether a URL
has been added before or not. A GET to an appropriate URL will return either
a 404 if the URL is not listed or a 204 if the URL is present. URLs that
it does not understand will return a 400 Bad Request.

This implementation will also match just the netloc part of a URL, if the
full URL is not found. So, if a URL is `http://localhost:90/a/path`, and the 
URL repository contains `http://localhost:90/`, then the URL with `a/path`
will match. Similarly, for URLs with query strings. If the URL in the
repository matches the given URL, but without the query string, the service
will return a 204.

## Questions answered

###### The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this VM?

I've implemented two possible solutions here.  One is to use redis cluster, and shard
the input URLs across the cluster. This allows the cluster to be scaled up as needed.

I've also implemented a Datastore solution when running in GCP. In this case, Google
scales Datastore automatically, and so no manual intervention is required. Costs
would increase, and this would have to be monitored separately.

The data storage is flexible in this application, with the in-memory URL storage used
for general development, and the others for data that exceeds a single machine.

###### The number of requests may exceed the capacity of this VM, how might you solve that?

Since the application layer is already in a container, I created a Kubernetes cluster in
GKE. The deployment yaml is in the gcp directory, but is fairly straightforward. While
running in GKE, the nodes run in a node pool that has a Kubernetes service account mapped
to a regular Google service account using the GCP 'Workload Identity'. This allows the
application to access Datastore using the given Google service account. Using Kubernetes,
we can specify how the deployment should scale and what metrics it should scale on. For
example, in my current deployment, there is 1 replica, and there can be a maximum of 5,
where it autoscales based on CPU usage.

The pods sit behind a single load balancer that provides the public IP address for the
service.

###### What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.

The update rate here could be much higher or much lower than the read rate from the
proxy. For this reason, I've allowed the application to run with or without the
admin APIs that allow creation and deletion of URLs. This allows me to have a
separately sized admin cluster that can service the PUT/DELETE requests, and also
allows any PUT/DELETE load to be independent of the GET requests (at the application
layer).

In the GCP version of this, users could also directly bulk import into Datastore,
either via the UI or via the command line. Similarly, users could add individual
entries directly into the Datastore. The problem with allowing this is that users
doing this need to know the internal data format. If a large volume of entries
needs to be added at once, I should add a command line option to the admin
application to do this.

If the number of updates is bursty, we may not want to expand the cluster to
handle the load. In this case, we could create a queue via GCP Pub/Sub to store
pending updates, and handle them at a fixed rate--perhaps by using a Cloud Function
that also used Redis (or Datastore) to implement rate limits.


## TODO

- add local cache
- add GCP Cloud SQL or Bigtable for data storage (for reduced latency)

## Development

Create the virtual environment and activate it:

    % make venv

You should also install the various requirements while in the virtual env:

    (venv)% make venv-deps

## Building

To generate an image, you can run:

    % make image

## Testing

### Unit tests

Unit tests can be run with the venv-test Makefile target, which will skip
the integration tests, as that requires some extra setup:

    (venv)% make venv-test

This will also output a coverage report.

### Integration tests

To run the integration tests, you must first create an image:

    % make image

This will generate an image tagged based on the version listed in
the `url_lookup/version.py` file and the current git short revision hash.
Copy the image tag into the `testing/integration/docker-compose-redis-cluster.yml` file:

      url-lookup:
        image: url_lookup:0.0.1-46191a9

You can then run the integration tests:

    (venv)% make venv-integration

## Admin API

You can add URLs by executing a PUT against the same URL you would query against.
So if you wanted:

    curl -vvv -X GET "localhost:8000/urlinfo/v1/localhost:80/a/path

to return a 204, you would have to PUT as follows:

    curl -vvv -X PUT "localhost:8000/urlinfo/v1/localhost:80/a/path

A successful PUT will return 201 CREATED.
Similarly, you can DELETE entries, which returns a 204 on success.
This assumes you have enabled the admin API for the application, which is the default.

### Manual testing

You can test things manually, with either the in-memory URL repository, or the
redis repository.

#### In-Memory URL Repository

You can modify the default config, in `url_lookup/config/defaults.py` to use the
in-memory repository:

    URL_REPOSITORY_MODULE = 'url_lookup.infra.inmemory_url_repository'
    URL_REPOSITORY_CLASS = 'InMemoryURLRepository'

Then you can run the server:

    (venv)% make venv-run

#### Redis Cluster URL Repository

You can modify the default config, in `url_lookup/config/defaults.py` to use the
redis cluster repository:

    REDIS_STARTUP_NODES = '[{"host": "127.0.0.1", "port": "6379"}]'
    URL_REPOSITORY_MODULE = 'url_lookup.infra.redis_cluster_url_repository'
    URL_REPOSITORY_CLASS = 'RedisClusterURLRepository'

You will need a redis cluster node listening on the given host and port.
Then you can run the server:

    (venv)% make venv-run

Alternatively, you can run the docker-compose script to start up the redis
cluster, and then attach another docker image to the cluster:

    (venv)% ./venv/bin/docker-compose -f testing/integration/docker-compose-redis-cluster.yml up -d redis-cluster-init
    (venv)% docker run --rm --network integration_default -it --entrypoint=/bin/bash url_lookup:0.0.1-46191a9

Using the image tag created from `make image`. From there, you can activate the virtual env
and run the server as you like.

#### Datastore URL Repository

This is only intended to be used when deployed to Google Kubernetes Engine. It
uses Datastore as a key-value store.
