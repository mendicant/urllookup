import urllib
from .base_url_repository import BaseURLRepository
from url_lookup.models import URL


class InMemoryURLRepository(BaseURLRepository):
    """
    A simple implementation of a URL repository, for testing
    """
    def __init__(self, _config):
        super().__init__()
        self._urls = set()

    def _generate_split_result(self, url: URL):
        return urllib.parse.SplitResult(
            scheme='',
            netloc=url.netloc,
            path=url.path,
            query=url.query,
            fragment='')

    def add(self, url: URL):
        parse_result = self._generate_split_result(url)
        full_url = urllib.parse.urlunsplit(parse_result)
        needed = (full_url not in self._urls)
        if needed:
            self._urls.add(full_url)
        return needed

    def get_url(self, url: URL):
        parse_result = self._generate_split_result(url)
        full_url = urllib.parse.urlunsplit(parse_result)
        return full_url in self._urls

    def delete(self, url: URL):
        parse_result = self._generate_split_result(url)
        full_url = urllib.parse.urlunsplit(parse_result)
        needed = (full_url in self._urls)
        if needed:
            self._urls.remove(full_url)
        return needed
