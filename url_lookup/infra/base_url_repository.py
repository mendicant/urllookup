from abc import ABC, abstractmethod


class BaseURLRepository(ABC):
    @abstractmethod
    def add(self, url):
        """
        Add a URL instance to the repository.
        Return True if added, and False if
        the url was already in the repository.
        """
        pass

    @abstractmethod
    def get_url(self, url):
        """
        Get a URL by netloc (ie: host:port), path and query
        The path and query may be None.

        Returns True if found and False if no such URL was found
        """
        pass

    @abstractmethod
    def delete(self, url):
        """
        Delete an entry

        Returns True if deleted and False otherwise
        """
        pass
