import json
from .base_url_repository import BaseURLRepository
from rediscluster import RedisCluster
from url_lookup.config.config_names import REDIS_STARTUP_NODES
from url_lookup.models import URL
from flask import current_app


class RedisClusterURLRepository(BaseURLRepository):
    """
    A URL Repository using redis cluster
    """
    def __init__(self, config):
        super().__init__()
        startup_nodes = json.loads(config[REDIS_STARTUP_NODES])
        self._client = RedisCluster(
            startup_nodes=startup_nodes, decode_responses=True)

    def _build_key(self, url):
        """
        Build the key to use for redis. The key will have a hash to
        prevent a single netloc from creating a hot shard.
        """
        # The key uses the reserved character '#' as a separator,
        # because we strip out any fragment part of the URL.
        return '{{}}ul#{netloc}#{path}#{query}'.format(
            netloc=url.netloc,
            path=url.path,
            query=url.query
        )

    def add(self, url: URL):
        """
        Add the given URL to the repo
        """
        current_app.logger.debug('Adding a url: {}'.format(url))
        key = self._build_key(url)
        result = self._client.set(key, 1, nx=True)
        return result is not None

    def get_url(self, url):
        """
        Check for existence of the URL's key in the repo
        Returns True if it exists and False otherwise
        """
        current_app.logger.debug('Checking a url: {}'.format(url))
        key = self._build_key(url)
        result = self._client.exists(key)
        return result == 1

    def delete(self, url):
        """
        Delete the URL from the repo
        Returns True if it existed and false otherwise
        """
        key = self._build_key(url)
        result = self._client.delete(key)
        return result == 1
