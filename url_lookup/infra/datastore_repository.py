from .base_url_repository import BaseURLRepository
from url_lookup.models import URL
from flask import current_app
from google.cloud import datastore


KIND = 'URL'


class DatastoreURLRepository(BaseURLRepository):
    """
    A URL Repository using GCP's datastore
    """
    def __init__(self, config):
        super().__init__()
        self._client = datastore.Client()

    def _build_key(self, url):
        """
        Build the key to use for redis. The key will have a hash to
        prevent a single netloc from creating a hot shard.
        """
        # The key uses the reserved character '#' as a separator,
        # because we strip out any fragment part of the URL.
        return '{{}}ul#{netloc}#{path}#{query}'.format(
            netloc=url.netloc,
            path=url.path,
            query=url.query
        )

    def add(self, url: URL):
        """
        Add the given URL to the repo
        """
        current_app.logger.debug('Adding a url: {}'.format(url))
        key = self._build_key(url)
        entity_key = self._client.key(KIND, key)
        with self._client.transaction():
            previous_entity = self._client.get(entity_key)
            if not previous_entity:
                entity = datastore.Entity(key=entity_key)
                entity['url'] = key
                self._client.put(entity)
        return previous_entity is None

    def get_url(self, url):
        """
        Check for existence of the URL's key in the repo
        Returns True if it exists and False otherwise
        """
        current_app.logger.debug('Checking a url: {}'.format(url))
        key = self._build_key(url)
        previous_entity = None
        with self._client.transaction():
            entity_key = self._client.key(KIND, key)
            previous_entity = self._client.get(entity_key)
        return previous_entity is not None

    def delete(self, url):
        """
        Delete the URL from the repo
        Returns True if it existed and false otherwise
        """
        key = self._build_key(url)
        entity_key = self._client.key(KIND, key)
        previous_entity = None
        with self._client.transaction():
            previous_entity = self._client.get(entity_key)
            self._client.delete(key)
        return previous_entity is not None
