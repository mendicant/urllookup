# For debug use
URL_REPOSITORY_MODULE = 'url_lookup.infra.inmemory_url_repository'
URL_REPOSITORY_CLASS = 'InMemoryURLRepository'
# Set to True to include the PUT and DELETE methods
INCLUDE_ADMIN_API = True

# For use with a redis cluster
REDIS_STARTUP_NODES = '[{"host": "127.0.0.1", "port": "6379"}]'
# URL_REPOSITORY_MODULE = 'url_lookup.infra.redis_cluster_url_repository'
# URL_REPOSITORY_CLASS = 'RedisClusterURLRepository'

# For use with GCP's datastore
# URL_REPOSITORY_MODULE = 'url_lookup.infra.datastore_repository'
# URL_REPOSITORY_CLASS = 'DatastoreURLRepository'
