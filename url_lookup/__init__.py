import http
import importlib
import injector
import os
from flask import Flask

from .service.lookup_service import LookupService
from .config.config_names import (
    URL_REPOSITORY_MODULE, URL_REPOSITORY_CLASS, INCLUDE_ADMIN_API)
from .resources.public_api import public_api_v1
from .resources.admin_api import admin_api_v1
from .infra.base_url_repository import BaseURLRepository
from flask_injector import FlaskInjector


class RepoModule(injector.Module):
    def __init__(self, app):
        self._app = app

    def configure(self, binder):
        repo_module = importlib.import_module(
            self._app.config[URL_REPOSITORY_MODULE])
        repo_class = getattr(
            repo_module, self._app.config[URL_REPOSITORY_CLASS])
        binder.bind(
            BaseURLRepository,
            to=repo_class(self._app.config),
            scope=injector.singleton)


class LookupServiceModule(injector.Module):
    def configure(self, binder):
        binder.bind(
            LookupService,
            to=self.create,
            scope=injector.singleton)

    @injector.inject
    def create(
        self, repo: BaseURLRepository
    ) -> LookupService:
        return LookupService(repo)


def create_app(test_config=None):
    app = Flask(__name__)
    app.config.from_object('url_lookup.config.defaults')
    if os.getenv('URL_LOOKUP_CONFIG') is not None:
        app.config.from_envvar('URL_LOOKUP_CONFIG')
    if test_config is not None:
        app.config.update(test_config)

    @app.route('/healthcheck', methods=['GET'])
    def get_healthcheck():
        return '', http.HTTPStatus.NO_CONTENT

    app.register_blueprint(public_api_v1)
    if app.config[INCLUDE_ADMIN_API]:
        app.register_blueprint(admin_api_v1)

    # Add a route for everything else, to distinguish between an
    # app 404 vs a request we don't understand
    @app.route('/', defaults={'u_path': ''})
    @app.route('/<path:u_path>')
    def catch_all(u_path):
        return '', http.HTTPStatus.BAD_REQUEST

    FlaskInjector(
        app=app,
        injector=injector.Injector([
            RepoModule(app),
            LookupServiceModule()]))
    return app
