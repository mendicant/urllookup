from url_lookup.models import URL


class LookupService(object):
    def __init__(self, url_repository):
        self._url_repository = url_repository

    def lookup(self, url):
        full_result = self._url_repository.get_url(url)
        if full_result:
            return full_result
        else:  # did not find full URL listed: check variations
            no_query_url = URL(
                netloc=url.netloc,
                path=url.path,
                query='')
            netloc_url = URL(
                netloc=url.netloc,
                path='',
                query='')
            return (
                self._url_repository.get_url(no_query_url) or
                self._url_repository.get_url(netloc_url))

    def add(self, url):
        return self._url_repository.add(url)

    def delete(self, url):
        return self._url_repository.delete(url)
