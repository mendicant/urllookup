class URL(object):
    """
    A URL, with path parameters embedded in path, and
    fragment and scheme ignored.
    """
    def __init__(self, netloc, path, query):
        """

        """
        self._netloc = netloc
        self._path = path
        self._query = query

    def __repr__(self):
        return 'URL(netloc={}, path={}, query={})'.format(
            self._netloc,
            self._path,
            self._query)

    @property
    def netloc(self):
        return self._netloc

    @property
    def path(self):
        return self._path

    @property
    def query(self):
        return self._query
