import http
import injector
from url_lookup.models import URL
from flask import Blueprint, request
from url_lookup.service.lookup_service import LookupService

# For future versioning
public_api_v1 = Blueprint(
    'public_version1', __name__, url_prefix='/urlinfo/v1')


@injector.inject
@public_api_v1.route(
    '/<netloc>/', defaults={'original_path': ''}, methods=['GET'])
@public_api_v1.route('/<netloc>/<path:original_path>', methods=['GET'])
def get_url(netloc, original_path, lookup_service: LookupService):
    """
    Check if the given URL has been listed. Returns NOT_FOUND if no
    such URL is in the repo and NO_CONTENT otherwise.
    """
    url = URL(
        netloc=netloc,
        path=original_path,
        query=request.query_string.decode('utf-8'))
    found = lookup_service.lookup(url)
    if found:
        code = http.HTTPStatus.NO_CONTENT
    else:
        code = http.HTTPStatus.NOT_FOUND
    return '', code
