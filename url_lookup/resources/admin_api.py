import http
import injector
from url_lookup.models import URL
from flask import Blueprint, request
from url_lookup.service.lookup_service import LookupService

admin_api_v1 = Blueprint('admin_version1', __name__, url_prefix='/urlinfo/v1')


@injector.inject
@admin_api_v1.route(
    '/<netloc>/', defaults={'original_path': ''}, methods=['PUT'])
@admin_api_v1.route('/<netloc>/<path:original_path>', methods=['PUT'])
def put_url(netloc, original_path, lookup_service: LookupService):
    """
    TODO: should go into admin
    """
    url = URL(
        netloc=netloc,
        path=original_path,
        query=request.query_string.decode('utf-8'))
    added = lookup_service.add(url)
    if added:
        code = http.HTTPStatus.CREATED
    else:
        code = http.HTTPStatus.OK
    return '', code


@injector.inject
@admin_api_v1.route(
    '/<netloc>/<path:original_path>',
    defaults={'original_path': ''},
    methods=['DELETE'])
@admin_api_v1.route('/<netloc>/<path:original_path>', methods=['DELETE'])
def delete_url(netloc, original_path, lookup_service: LookupService):
    """
    TODO: should go into admin
    """
    url = URL(
        netloc=netloc,
        path=original_path,
        query=request.query_string.decode('utf-8'))
    deleted = lookup_service.delete(url)
    if deleted:
        code = http.HTTPStatus.NO_CONTENT
    else:
        code = http.HTTPStatus.NOT_FOUND
    return '', code
