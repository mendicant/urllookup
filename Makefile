PROJECT_ID=url-lookup-287717
APP_VERSION=$(shell python3 -c "from url_lookup.version import __version__; print(__version__)")
GIT_REV=$(shell git rev-parse --short HEAD)

venv:
	if [ ! -d venv ]; then python3 -m venv venv; fi
	. venv/bin/activate

venv-deps:
	pip install -r requirements.txt
	pip install -r requirements-testing.txt

.PHONY: image
image:
	docker build --no-cache -t url_lookup:${APP_VERSION}-${GIT_REV} -f container/Dockerfile .

.PHONY: gcpimage
gcpimage:
	docker build --no-cache -t gcr.io/${PROJECT_ID}/urllookup:${APP_VERSION}-${GIT_REV} -f container/Dockerfile .

.PHONY: venv-test
venv-test:
	./venv/bin/pytest -m "not integration" --cov=url_lookup

# You must update the docker-compose-redis-cluster.yml image name before running integration tests
.PHONY: venv-integration
venv-integration:
	./venv/bin/pytest -m integration

.PHONY: venv-run
venv-run:
	./venv/bin/gunicorn "url_lookup:create_app()"
